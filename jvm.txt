jvm相关：

 原码 反码  补码  http://blog.csdn.net/xiaohai0504/article/details/6925553  

正数 没有反码，补码 ；负数的反码，符号位不变，数值取反； 负数的反码加1（会影响符号位）；单字节

-128是没有原码和反码的，只有补码！

2.  浮点数：IEEE754   http://www.cnblogs.com/kingwolfofsky/archive/2011/07/21/2112299.html

       两种基本的浮点格式：单精度和双精度。(其中有效数字都有一个最高位时符号位)

IEEE 单精度格式具有 24 位有效数字精度，并总共占用 32 位。1符号位 | 8指数位（没有使用反补码，使用+127保存） | 23尾数位（肯定小数点位1，省略）



IEEE 双精度格式具有 53 位有效数字精度，并总共占用 64 位。1符号位 | 11指数位（没有使用反补码，使用+1023保存） | 52尾数位（肯定小数点位1，省略）



3.  JVM 运行机制  http://my.oschina.net/jielucky/blog/170307

     jvm分为：pc寄存器，Class loader，Runtime Engine，native Methed interface；GC

     Runtime Engine： java stack； java heap；java methed space；java native methed interface；

     java stack：线程独享； 操作组成栈， 操作数栈(可以设置逃逸优化，让对象在stack上分配，达到无锁优化)

     java heap：需要gc，线程共享；volitate 和 锁可以让主存操作序列化。

     java methed space ：存放加载累的字节码，存放在 perm Generation。

4.  JVM 内存参数设置：http://www.blogjava.net/huanghuizz/articles/287127.html

-Xmx  heap max size ; -Xms heap min size; -Xmn heap yong Generation size; -Xss stack size every thread(多线程可以设置小一些，有递归可以设置大一些)；-XX:PermSize Perm Generation min size;  -XX:MaxPermSize Perm Generation max size;  



-XX:NewRatio  yong Generation / old Generation (CMS GC 下失效); -XX:SurvivorRatio  Eden Space / one of Survivor Space , for example :   -XX:SurvivorRatio=8, if yong Generation is 10M, the Eden is 8M; -XX:LargePageSizeInBytes heap page size; -XX:MinHeapFreeRatio 如果小于这个值，heap压缩，-XX:MinHeapFreeRatio 如果大于这个值，heap扩张，以上在 Xmx = Xms时候失效。



-verbose:gc;  -XX:+printGC; +XX:+printGCDetails; +XX:+printGCTimeStamps; -X:loggc:log/loggc.log;

+XX:+traceClassLoading; +XX:+PrintClassHistogram; 

+XX:+HeapDumpOnOutMemeryError; +XX:HeapDumpPath;+XX:+OnOutMemeryError;-XX:ErrorFile=./hs_err_pid<pid>.log(http://my.oschina.net/xionghui/blog/498785)

内存分析，java 支配树与MAT工具使用 http://book.51cto.com/art/201504/472189.htm#book_content

5.  JAVA GC 及 参数: （heap and perm）

GC算法（http://www.blogjava.net/showsun/archive/2011/07/21/354745.html）： 引用计数（python as3 等）； 标记清除； 标记压缩； 复制算法。 后三个都以root为查找

gc容易引起STW（stop the world）

分带： 青年代， 老年代

http://www.importnew.com/13827.html  http://www.cnblogs.com/hnrainll/archive/2013/11/06/3410042.html

参数：

-XX:+UseSerialGC 新生代，老年代串行回收，新生代 复制算法，老年代标记-压缩。

-XX:+UseParNewGC （--XX:ParallelGCThreads 限制线程数量） 新生代并行， 老年代串行； 算法与上面一样。

-XX:+UseParallelGC 关注吞吐，老年代串行， -XX:+UseParallelOldGC 使用UseParallelGC + 并行老年代。

-XX:maxGCPauseMills GC尽量不超过此值， -XX:GCTimeRatio gc时间比，默认99，既1%时间做GC

CMS： -XX:+UseConcMarkSweepGC  Concurrent Mark Sweep 并发标记清除；新生代copy，并发（Full gc时）初始标记；并发标记；重新标记；并发清除; 重置。降低停顿带来碎片 和 清理不测底、 

http://blog.csdn.net/fenglibing/article/details/6321453

G1： -XX:+UseG1GC g1垃圾收集器。初始标记； 扫描根区； 并发标记；再次标记；清理； 复制。

http://blog.csdn.net/renfufei/article/details/41897113

6.java类加载器与字节码：

类装置流程： 

加载： 取得类的二进制流，转为方法区数据结构，在Java堆中生成对应的java.lang.Class对象。

链接 ：

    验证：文件格式：1）是否0xCAFEBABE开头，版本号是否合理 2）版本号是否合理

              元数据验证 1）是否有父类  2）继承了final类 ？ 3）非抽象类实现了所有的抽象方法

               字节码验证（很复杂） 1）运行检查 2）栈数据类型和操作码数据参数吻合 3）跳转指令指定到合理的位置

    准备：分配内存，并为类设置初始值 （方法区中） final static的初始值为赋值，非final的默认值。

    解析 ：符号引用替换为直接引用

初始化：

    执行类构造器<clinit> static 变量 赋值语句； static {} 语句

    子类的<clinit>调用前保证父类的<clinit>被调用

     <clinit>是线程安全的

ClassLoader ： BootStrap ClassLoader ; Extension ClassLoader； App ClassLoader ； Custom ClassLoader

class结构 http://wiki.jikexueyuan.com/project/java-vm/class.html

深入了解Java ClassLoader、Bytecode 、ASM、cglib （http://www.iteye.com/topic/98178）

http://github.thinkingbar.com/categories/#java



7.锁

http://icyfenix.iteye.com/blog/1018932

http://wenku.baidu.com/view/bafd15f57c1cfad6195fa71c

http://www.infoq.com/cn/articles/java-se-16-synchronized

http://valleylord.sinaapp.com/post/201410-java-lock





算法相关：

具体案例：

车绕场一周，正好油桶够跑一周的油，请问是否可以找到一个油桶开始找到跑一周？ -- 如何找，最佳 n

i=s=0; while (s<n) s+=i++  -- 复杂度根号n

100 个灯  100个人，倍数可按开关  多少亮，多少灭？   --  合数总有两个数相乘，唯有乘数相等的特殊。





算法：

动态规划 贪心



KMP（http://www.cnblogs.com/goagent/archive/2013/05/16/3068442.html）

   j         0  1  2  3  4  5  6  7

   P        a   b  a  a  b  c  a   c

next[j]  -1  0  0  1  1  2  0  1

通过跳跃表，直接实现 一次遍历，无需回溯。



LCS（http://blog.chinaunix.net/uid-26548237-id-3374211.html）

·m和n分别从0开始，m++，n++循环：

       - 如果str1[m] == str2[n]，则L[m,n] = L[m - 1, n -1] + 1；

       - 如果str1[m] != str2[n]，则L[m,n] = max{L[m,n - 1]，L[m - 1, n]

1,2,3,4 四状态回溯可以支持多个lcs串



Dijkstra（http://www.cnblogs.com/dolphin0520/archive/2011/08/26/2155202.html）

假设P(i,j)={Vi....Vk..Vs...Vj}是从顶点i到j的最短路径，则有P(i,j)=P(i,k)+P(k,s)+P(s,j)。对于源顶点V0，首先选择其直接相邻的顶点中长度最短的顶点Vi，那么当前已知可得从V0到达Vj顶点的最短距离dist[j]=min{dist[j],dist[i]+matrix[i][j]}

?